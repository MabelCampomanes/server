// Conexión con MongoAtlass | MongoDB Compass 
const mongoose = require('mongoose');
require('dotenv').config();

const DB_URL = process.env.DB_URL || 'mongodb+srv://Mabel:9611NewYork@atlascluster.xp8lho9.mongodb.net/';

const connect = () => mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

module.exports = {
    DB_URL,
    connect
  };
  